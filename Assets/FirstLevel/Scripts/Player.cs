﻿using UnityEngine.Networking;
using UnityEngine.Events;
using UnityEngine;

[System.Serializable]
public class ToggleEvent : UnityEvent<bool> { }

public class Player : NetworkBehaviour
{
    
    [SerializeField]
    ToggleEvent onToggleShared;
    [SerializeField]
    ToggleEvent onToggleLocal;
    [SerializeField]
    ToggleEvent onToggleRemote;
    [SerializeField]
    private float respawnTime = 5;

    private NetworkAnimator anim;

    public SkinnedMeshRenderer[] skinMeshes;

    private void Start()
    {
        EnablePlayer();
        anim = GetComponent<NetworkAnimator>();
    }

    public override void OnStartLocalPlayer()
    {
        
    }

    void Update()
    {
        if(!isLocalPlayer)
            return;
    }

    public void DisablePlayer()
    {
        onToggleShared.Invoke(false);
        if (isLocalPlayer)
            onToggleLocal.Invoke(false);
        else
        {
            onToggleRemote.Invoke(false);
        }
    }


    public void EnablePlayer()
    {
        onToggleShared.Invoke(true);
        if(isLocalPlayer)
            onToggleLocal.Invoke(true);
        else
        {
            onToggleRemote.Invoke(true);
        }
    }


    public void Die()
    {

        if (isLocalPlayer)
            anim.SetTrigger("Died");

        DisablePlayer();

        Invoke("Respawn",respawnTime);
    }

    void Respawn()
    {
        if (isLocalPlayer)
        {
            Transform spawn = NetworkManager.singleton.GetStartPosition();
            transform.position = spawn.position;
            transform.rotation = spawn.rotation;

            anim.SetTrigger("Restart");

        }
        EnablePlayer();
    }
}
