﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Contains the command the user wishes upon the character
struct Cmd
{
    public float forwardMove;
    public float rightMove;
    public float upMove;
}


public class QuakePlayerMovement : MonoBehaviour
{

    private Transform playerView;     // Camera
    public float playerViewYOffset = 0.6f; // The height at which the camera is bound to
    public float xMouseSensitivity = 60.0f;
    public float yMouseSensitivity = 100.0f;
    //
    /*Frame occuring factors*/
    public float gravity = 20.0f;

    public float friction = 6; //Ground friction

    /* Movement stuff */
    public float moveSpeed = 7.0f;                // Ground move speed
    public float runAcceleration = 14.0f;         // Ground accel
    public float runDeacceleration = 10.0f;       // Deacceleration that occurs when running on the ground
    public float airAcceleration = 0.05f;          // Air accel
    public float airDecceleration = 2.0f;         // Deacceleration experienced when ooposite strafing
    public float airControl = 0.3f;               // How precise air control is
    public float sideStrafeAcceleration = 50.0f;  // How fast acceleration occurs to get up to sideStrafeSpeed when
    public float sideStrafeSpeed = 1.0f;          // What the max speed to generate when side strafing
    public float jumpSpeed = 8.0f;                // The speed at which the character's up axis gains when hitting jump
    public float moveScale = 1.0f;


    /*print() style */
    public GUIStyle style;

    /*FPS Stuff */
    public float fpsDisplayRate = 4.0f; // 4 updates per sec

    private int frameCount = 0;
    private float dt = 0.0f;
    private float fps = 0.0f;

    private CharacterController controller;

    // Camera rotations
    private float rotX = 0.0f;
    private float rotY = 0.0f;

    private Vector3 moveDirection;
    //private Vector3 moveDirectionNorm = Vector3.zero;
    private Vector3 playerVelocity = Vector3.zero;
    private float playerTopVelocity = 0.0f;

    // If true then the player is fully on the ground
    //private bool grounded = false;

    // Q3: players can queue the next jump just before he hits the ground
    private bool wishJump = false;

    // Used to display real time fricton values
    //private float playerFriction = 0.0f;

    // Player commands, stores wish commands that the player asks for (Forward, back, jump, etc)
    private Cmd cmd;


    /* Player statuses */
    //private bool isDead = false;

    private Vector3 playerSpawnPos;
    private Quaternion playerSpawnRot;


    public bool showDebugText = false;


    public delegate void playerJumped();
    public playerJumped playerJump;


    // Use this for initialization
    void Start()
    {
        //Get The Camera
        playerView = GetComponent<InstantiateACamera>().GetCamera();

        // Hide the cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;

        // Put the camera inside the capsule collider
        playerView.position = new Vector3(
            transform.position.x,
            transform.position.y + playerViewYOffset,
            transform.position.z);

        controller = GetComponent<CharacterController>();

        // Set the spawn position of the player
        playerSpawnPos = transform.position;
        playerSpawnRot = this.playerView.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        // Do FPS calculation
        frameCount++;
        dt += Time.deltaTime;
        if (dt > 1.0 / fpsDisplayRate)
        {
            fps = Mathf.Round(frameCount / dt);
            frameCount = 0;
            dt -= 1.0f / fpsDisplayRate;
        }
        /* Ensure that the cursor is locked into the screen */
        if (Cursor.lockState != CursorLockMode.Locked)
        {
            if (Input.GetMouseButtonDown(0))
                Cursor.lockState = CursorLockMode.Locked;
        }


        /* Camera rotation stuff, mouse controls this shit */
        rotX -= Input.GetAxis("Mouse Y") * xMouseSensitivity * 0.02f;
        rotY += Input.GetAxis("Mouse X") * yMouseSensitivity * 0.02f;

        // Clamp the X rotation
        if (rotX < -90)
            rotX = -90;
        else if (rotX > 90)
            rotX = 90;

        

        

        /* Movement, here's the important part */
        QueueJump();
        if (controller.isGrounded)
            GroundMove();
        else if (controller.isGrounded == false)
            AirMove();

        // Move the controller
        controller.Move(playerVelocity * Time.deltaTime);

        transform.rotation = Quaternion.Euler(0, rotY, 0); // Rotates the collider
        playerView.rotation = Quaternion.Euler(rotX, rotY, 0); // Rotates the camera

        // Set the camera's position to the transform
        playerView.position = new Vector3(
            transform.position.x,
            transform.position.y + playerViewYOffset,
            transform.position.z);

        /* Calculate top velocity */
        Vector3 udp = playerVelocity;
        udp.y = 0.0f;
        if (playerVelocity.magnitude > playerTopVelocity)
            playerTopVelocity = playerVelocity.magnitude;
    }

#region Movement

    /**
     * Sets the movement direction based on player input
     */
    private void SetMovementDir()
    {
        cmd.forwardMove = Input.GetAxisRaw("Vertical");
        cmd.rightMove = Input.GetAxisRaw("Horizontal");
    }

    /**
     * Queues the next jump just like in Q3
     */
    private void QueueJump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !wishJump)
            wishJump = true;
        if (Input.GetKeyUp(KeyCode.Space))
            wishJump = false;
    }


    /**
     * Executess when the player is in the air
    */
    private void AirMove()
    {
        Vector3 wishdir;
        //float wishvel = airAcceleration;
        float accel;

        float scale = CmdScale();

        SetMovementDir();

        wishdir = new Vector3(cmd.rightMove, 0, cmd.forwardMove);
        wishdir = transform.TransformDirection(wishdir);

        float wishspeed = wishdir.magnitude;
        wishspeed *= moveSpeed;

        wishdir.Normalize();
       // moveDirectionNorm = wishdir;
        wishspeed *= scale;

        // CPM: Aircontrol
        var wishspeed2 = wishspeed;
        if (Vector3.Dot(playerVelocity, wishdir) < 0)
            accel = airDecceleration;
        else
            accel = airAcceleration;
        // If the player is ONLY strafing left or right
        if (cmd.forwardMove == 0 && cmd.rightMove != 0)
        {
            if (wishspeed > sideStrafeSpeed)
                wishspeed = sideStrafeSpeed;
            accel = sideStrafeAcceleration;
        }

        Accelerate(wishdir, wishspeed, accel);
        if (airControl > 0)
            AirControl(wishdir, wishspeed2);
        // !CPM: Aircontrol

        // Apply gravity
        playerVelocity.y -= gravity * Time.deltaTime;

        // LEGACY MOVEMENT SEE BOTTOM

    }

    /**
 * Air control occurs when the player is in the air, it allows
 * players to move side to side much faster rather than being
 * 'sluggish' when it comes to cornering.
 */
    private void AirControl(Vector3 wishdir, float wishspeed)
    {

        float zspeed;
        float speed;
        float dot;
        float k;
        //int i;

        // Can't control movement if not moving forward or backward
        if (cmd.forwardMove == 0 || wishspeed == 0)
            return;

        zspeed = playerVelocity.y;
        playerVelocity.y = 0;
        /* Next two lines are equivalent to idTech's VectorNormalize() */
        speed = playerVelocity.magnitude;
        playerVelocity.Normalize();

        dot = Vector3.Dot(playerVelocity, wishdir);
        k = 32;
        k *= airControl * dot * dot * Time.deltaTime;

        // Change direction while slowing down
        if (dot > 0)
        {
            playerVelocity.x = playerVelocity.x * speed + wishdir.x * k;
            playerVelocity.y = playerVelocity.y * speed + wishdir.y * k;
            playerVelocity.z = playerVelocity.z * speed + wishdir.z * k;

            playerVelocity.Normalize();
            //moveDirectionNorm = playerVelocity;
        }

        playerVelocity.x *= speed;
        playerVelocity.y = zspeed; // Note this line
        playerVelocity.z *= speed;

    }

    /**
 * Called every frame when the engine detects that the player is on the ground
 */
    private void GroundMove()
    {
        Vector3 wishdir;
        // wishvel;

        // Do not apply friction if the player is queueing up the next jump
        if (!wishJump)
            ApplyFriction(1.0f);
        else
            ApplyFriction(0);

        //var scale = CmdScale();

        SetMovementDir();

        wishdir = new Vector3(cmd.rightMove, 0, cmd.forwardMove);
        wishdir = transform.TransformDirection(wishdir);
        wishdir.Normalize();
        //moveDirectionNorm = wishdir;

        var wishspeed = wishdir.magnitude;
        wishspeed *= moveSpeed;

        Accelerate(wishdir, wishspeed, runAcceleration);

        // Reset the gravity velocity
        playerVelocity.y = 0;

        if (wishJump)
        {
            //We jump here
            if(playerJump != null)
                playerJump();

            playerVelocity.y = jumpSpeed;
            wishJump = false;
        }
    }

    /**
 * Applies friction to the player, called in both the air and on the ground
 */
    private void ApplyFriction(float t)
    {
        Vector3 vec = playerVelocity; // Equivalent to: VectorCopy();
        //float vel;
        float speed;
        float newspeed;
        float control;
        float drop;

        vec.y = 0.0f;
        speed = vec.magnitude;
        drop = 0.0f;

        /* Only if the player is on the ground then apply friction */
        if (controller.isGrounded)
        {
            control = speed < runDeacceleration ? runDeacceleration : speed;
            drop = control * friction * Time.deltaTime * t;
        }

        newspeed = speed - drop;
        //playerFriction = newspeed;
        if (newspeed < 0)
            newspeed = 0;
        if (speed > 0)
            newspeed /= speed;

        playerVelocity.x *= newspeed;
        // playerVelocity.y *= newspeed;
        playerVelocity.z *= newspeed;
    }

    /**
 * Calculates wish acceleration based on player's cmd wishes
 */
    private void Accelerate(Vector3 wishdir, float wishspeed, float accel)
    {
        float addspeed;
        float accelspeed;
        float currentspeed;

        currentspeed = Vector3.Dot(playerVelocity, wishdir);
        addspeed = wishspeed - currentspeed;
        if (addspeed <= 0)
            return;
        accelspeed = accel * Time.deltaTime * wishspeed;
        if (accelspeed > addspeed)
            accelspeed = addspeed;

        playerVelocity.x += accelspeed * wishdir.x;
       
        playerVelocity.z += accelspeed * wishdir.z;
    }

    public void OutsideAcceleration(Vector3 wishDir, float wishSpeed, float accel)
    {
        float addSpeed, accelSpeed, currentSpeed;

        currentSpeed = Vector3.Dot(playerVelocity, wishDir);
        addSpeed = wishSpeed - currentSpeed;

        if (addSpeed <= 0)
            return;
        accelSpeed = accel * Time.deltaTime * wishSpeed;
        if (accelSpeed > addSpeed)
            accelSpeed = addSpeed;

        playerVelocity.x += accelSpeed * wishDir.x;
        playerVelocity.y += accelSpeed * wishDir.y;
        playerVelocity.z += accelSpeed * wishDir.z;
    }


#endregion

    /*
============
PM_CmdScale
Returns the scale factor to apply to cmd movements
This allows the clients to use axial -127 to 127 values for all directions
without getting a sqrt(2) distortion in speed.
============
*/
    private float CmdScale()
    {
        int max;
        float total;
        float scale;

        max = (int)Mathf.Abs(cmd.forwardMove);
        if (Mathf.Abs(cmd.rightMove) > max)
            max = (int)Mathf.Abs(cmd.rightMove);
        if (max <= 0)
            return 0;

        total = Mathf.Sqrt(cmd.forwardMove * cmd.forwardMove + cmd.rightMove * cmd.rightMove);
        scale = moveSpeed * max / (moveScale * total);

        return scale;
    }

    void PlayerExplode()
    {
        //var velocity = controller.velocity;
        //velocity.Normalize();
        //var gibEffect = Instantiate(gibEffectPrefab, transform.position, Quaternion.identity);
        //gibEffect.GetComponent(GibFX).Explode(transform.position, velocity, controller.velocity.magnitude);
        //isDead = true;
    }

    void PlayerSpawn()
    {
        this.transform.position = playerSpawnPos;
        this.playerView.rotation = playerSpawnRot;
        rotX = 0.0f;
        rotY = 0.0f;
        playerVelocity = Vector3.zero;
        //isDead = false;
    }
    public void ResetPlayerVelocity()
    {
        playerVelocity = Vector3.zero;
    }


    

    private void OnGUI()
    {
        if (showDebugText)
        {
            GUI.Label(new Rect(0, 0, 400, 100), "FPS: " + fps, style);
            var ups = controller.velocity;
            ups.y = 0;
            GUI.Label(new Rect(0, 15, 400, 100), "Speed: " + Mathf.Round(ups.magnitude * 100) / 100 + "ups", style);
            GUI.Label(new Rect(0, 30, 400, 100), "Top Speed: " + Mathf.Round(playerTopVelocity * 100) / 100 + "ups", style);
        }
    }

    // Legacy movement

    // var wishdir : Vector3;
    // var wishvel : float = airAcceleration;

    // // var scale = CmdScale();

    // SetMovementDir();

    // /* If the player is just strafing in the air 
    //    this simulates CPM (Not very accurately by
    //    itself) */
    // if(cmd.forwardmove == 0 && cmd.rightmove != 0)
    // {
    // 	wishvel = airStrafeAcceleration;
    // }

    // wishdir = Vector3(cmd.rightmove, 0, cmd.forwardmove);
    // wishdir = transform.TransformDirection(wishdir);
    // wishdir.Normalize();
    // moveDirectionNorm = wishdir;

    // var wishspeed = wishdir.magnitude;
    // wishspeed *= moveSpeed;

    // Accelerate(wishdir, wishspeed, wishvel);

    // // Apply gravity
    // playerVelocity.y -= gravity * Time.deltaTime;

}
