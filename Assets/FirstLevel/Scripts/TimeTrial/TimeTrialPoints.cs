﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeTrialPoints : MonoBehaviour
{
    public bool endOfTrial;
    public bool startOfTrial;

    private void OnTriggerEnter(Collider other)
    {
        if(startOfTrial)
        {
            //We are inside a trigger that should start the clock
            if (other.CompareTag("Player"))
            {
                TimeTrialMode.instance.StartedTrial(other.gameObject);
            }
        }
        if (endOfTrial)
        {
            //We finished the trial
            if (other.CompareTag("Player"))
            {
                TimeTrialMode.instance.FinishedTrial(other.gameObject);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(startOfTrial)
        {
            //We Left the trigger this should start the clock
            if(other.CompareTag("Player"))
            {
                TimeTrialMode.instance.StartedTrial(other.gameObject);
            }
        }
    }
}
