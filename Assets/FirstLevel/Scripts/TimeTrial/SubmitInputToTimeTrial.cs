﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubmitInputToTimeTrial : MonoBehaviour
{
    private void OnEnable()
    {
        GetComponent<InputField>().Select();
    }
    public void CallTimeTrialMode(InputField UIField)
    {
        TimeTrialMode.instance.InputtedName(UIField);
    }
}
