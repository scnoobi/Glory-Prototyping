﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Details
{
    public float runningTime;
    public string runnersName;
}

public class Leaderboard : MonoBehaviour
{
    public Transform leaderboardCanvas;
    public Transform canvas;

    public Transform playerItem;

    private List<Details> playerDetails = new List<Details>();

    public static Leaderboard instance;

    private void Awake()
    {
        instance = this;
        leaderboardCanvas.gameObject.SetActive(false);
    }
    public void NewPlayer(float timeTakenForLevel, string runnerName)
    {
        Details aPlayer = new Details()
        {
            runningTime = timeTakenForLevel,
            runnersName = runnerName
        };

        playerDetails.Add(aPlayer);

        Instantiate(playerItem, canvas);

        QuickSort.Quicksort(playerDetails, 0, playerDetails.Count - 1);

        UpdateLeaderboardText();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            /*
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            */
            leaderboardCanvas.gameObject.SetActive(true);

        }
        if (Input.GetKeyUp(KeyCode.Tab))
        {
            /*Cursor.visible = false;
#if UNITY_EDITOR
            Cursor.lockState = CursorLockMode.Locked;
#else
            Cursor.lockState = CursorLockMode.Confined;
#endif
*/
            leaderboardCanvas.gameObject.SetActive(false);
        }
    }

    private void UpdateLeaderboardText()
    {
        for (int i = 0; i < playerDetails.Count; i++)
        {
            canvas.transform.GetChild(i).GetChild(0).GetComponent<Text>().text = playerDetails[i].runnersName;
            canvas.transform.GetChild(i).GetChild(1).GetComponent<Text>().text = playerDetails[i].runningTime.ToString();
        }
    }
}
