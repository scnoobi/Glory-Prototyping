﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayerInsideBounds : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            TimeTrialMode.instance.GoToLatestCheckPoint(other.gameObject);
        }
    }
}
