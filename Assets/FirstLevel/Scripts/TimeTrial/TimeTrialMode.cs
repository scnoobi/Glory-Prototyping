﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Runners
{
    public float timeStarted;
    public GameObject runner;
    public float timeTakeToFinish;
}
public class TimeTrialMode : MonoBehaviour
{
    public CheckPoint[] checkPoints;

    public static TimeTrialMode instance;

    private List<Runners> playersCurrentlyRunning = new List<Runners>();

    private float timer;
    private bool shouldTheTimerRun = false;

    

    private void Awake()
    {
        playersCurrentlyRunning.Clear();
        instance = this;
    }

    private void Start()
    {
        //checkPoints = FindObjectsOfType<CheckPoint>();
        /*if (checkPoints.Length < 1)
            Debug.LogError("Couldn't find a single checkpoint");
            */
    }

    // Update is called once per frame
    void Update()
    {
        if (shouldTheTimerRun)
        {
            timer += Time.deltaTime;
        }
    }
    public void StartedTrial(GameObject playerThatStarted)
    {
        //Do a check if we already have this runner. if so remove that runner
        foreach (var player in playersCurrentlyRunning)
        {
            if(player.runner == playerThatStarted)
            {
                playersCurrentlyRunning.Remove(player);
                break;
            }
        }


        //If no1 has started running then start the timer
        if (playersCurrentlyRunning.Count == 0)
        {
            StartTimer();
        }

        Runners aRunner = new Runners();
        aRunner.timeStarted = timer;
        aRunner.runner = playerThatStarted;

        playersCurrentlyRunning.Add(aRunner);

    }
    public void FinishedTrial(GameObject playerThatFinished)
    {
        //find our player and then Activate their UI
        playerThatFinished.transform.GetComponentInChildren<SubmitInputToTimeTrial>(true).gameObject.SetActive(true);

        foreach (var player in playersCurrentlyRunning)
        {
            if (player.runner == playerThatFinished)
            {
                player.timeTakeToFinish = timer - player.timeStarted;
                break;
            }
        }

    }

    private void StartTimer()
    {
        timer = 0;
        shouldTheTimerRun = true;
    }
    private void StopTimer()
    {
        shouldTheTimerRun = false;
        timer = 0;
    }

    private void SubmitATime(GameObject player, float timeWhenFinished)
    {
        Leaderboard.instance.NewPlayer(timeWhenFinished, player.name);

        //Removing the player

        foreach (var aPlayer in playersCurrentlyRunning)
        {
            if (aPlayer.runner == player)
            {
                playersCurrentlyRunning.Remove(aPlayer);
                break;
            }
        }

        //If all players have finished. we should stop the timer
        if (playersCurrentlyRunning.Count == 0)
        {
            StopTimer();
        }
    }

    public void GoToLatestCheckPoint(GameObject thePlayer)
    {
        thePlayer.transform.position = checkPoints[0].transform.position;
        thePlayer.GetComponent<QuakePlayerMovement>().ResetPlayerVelocity();
    }
    public void InputtedName(InputField playerInputField)
    {
        var playerController = playerInputField.transform.parent.parent.gameObject;
        playerController.name = playerInputField.text;


        Runners finishedRunner = new Runners();
        foreach (var player in playersCurrentlyRunning)
        {
            if (player.runner == playerController)
            {
                finishedRunner = player;
                break;
            }
        }

        playerInputField.gameObject.SetActive(false);

        if (finishedRunner.runner != null)
        {
            SubmitATime(finishedRunner.runner, finishedRunner.timeTakeToFinish);
            
        }
        else
        {
            Debug.Log("Finished runner is null");
        }

        //we have done everything we can at the end. we should spawn the player at the start to try again
        GoToLatestCheckPoint(playerController);
    }
}
