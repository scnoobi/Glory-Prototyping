﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour
{

    private void Start()
    {
        Destroy(gameObject, 10);
    }
    public void Fire(float speed, AerialFire originator)
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * speed, ForceMode.Impulse);
    }
}
