﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSounds : MonoBehaviour
{
    [SerializeField]
    private AudioClip jumpingAudioClip;

    private AudioSource audioSource;

    private QuakePlayerMovement playerMovement;
    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        playerMovement = GetComponent<QuakePlayerMovement>();
        playerMovement.playerJump += PlaySound;
    }
    private void OnDisable()
    {
        playerMovement.playerJump -= PlaySound;
    }
    void PlaySound()
    {
        audioSource.clip = jumpingAudioClip;
        audioSource.Play();
    }
}
