﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiManager : MonoBehaviour
{
    public Transform pauseMenu;

    private bool pauseMenuActive = false;
	// Use this for initialization
	void Start ()
    {
        pauseMenu.gameObject.SetActive(pauseMenuActive);
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
        }
	}

    public void QuitGame()
    {
        #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
        #else
                Application.Quit();
        #endif
    }

    void PauseGame()
    {
        pauseMenuActive = !pauseMenuActive;
        pauseMenu.gameObject.SetActive(pauseMenuActive);

        var playerComponent = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        if(pauseMenuActive)
        {
            playerComponent.DisablePlayer();
            Cursor.visible = true;
        }
        else
        {
            playerComponent.EnablePlayer();
            Cursor.visible = false;
        }
        
    }
}
