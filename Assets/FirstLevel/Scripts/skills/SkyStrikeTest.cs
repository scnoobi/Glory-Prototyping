﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyStrikeTest : MonoBehaviour
{

    public LayerMask whoICanHit;

    private List<Rigidbody> everyoneInsideSpell = new List<Rigidbody>();

    private Vector3 mouseStartPosition;

    public float manipulationForce = 7;
    [Tooltip("how far up enemies will go when they are hit by the skill")]
    public float enemyUpForce = 8;
    [Tooltip("The max amount that you can toss someone")]
    public float maxForceOnEnemy = 7;

    public float amountToDivideTheSpeedOn = 500;

    //this determines how long the player has time to manipulate the skills effects.
    private float durationOfSkillInput = 0.1f;
    private float timer;

    // Use this for initialization
    void Start ()
    {
        mouseStartPosition = Input.mousePosition;

        var everyoneInsideSphere = Physics.OverlapSphere(transform.position, 2);
        
        for (int i = 0; i < everyoneInsideSphere.Length; i++)
        {
            Debug.Log(i);
            if (everyoneInsideSphere[i].GetComponent<Rigidbody>() != null)
            {
                everyoneInsideSpell.Add(everyoneInsideSphere[i].GetComponent<Rigidbody>());
            }
        }
        StartCoroutine(Movement());
    }

    IEnumerator Movement()
    {
        for (int i = 0; i < everyoneInsideSpell.Count; i++)
        {
            everyoneInsideSpell[i].AddForce(Vector3.up * enemyUpForce, ForceMode.Impulse);
        }
        
        while (timer < durationOfSkillInput)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        var mouseEndPosition = Input.mousePosition;
        var directionAndLength = (mouseEndPosition - mouseStartPosition);
        
        directionAndLength.z = directionAndLength.y;
        directionAndLength.y = 0;

        var mouseSpeed = directionAndLength.magnitude / amountToDivideTheSpeedOn;
        var direction = directionAndLength.normalized;

        Debug.Log("mouse Speed: " + mouseSpeed);

        var enemyFlyingSpeed = Vector3.ClampMagnitude(mouseSpeed * manipulationForce * direction, maxForceOnEnemy);

        var flyingDirection = transform.forward * enemyFlyingSpeed.z + transform.right * enemyFlyingSpeed.x;

        //Applying the playerManipulation
        for (int i = 0; i < everyoneInsideSpell.Count; i++)
        {
            everyoneInsideSpell[i].AddForce(flyingDirection,ForceMode.Impulse);
        }

        Destroy(gameObject,0.4f);
    }
}
