﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatingASpell : MonoBehaviour
{
    public Transform InFrontOfThePlayer;
    public GameObject spell;
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Instantiate(spell, InFrontOfThePlayer.position, InFrontOfThePlayer.rotation);
        }
	}
}
