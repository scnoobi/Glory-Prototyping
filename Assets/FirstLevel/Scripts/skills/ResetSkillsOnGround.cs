﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetSkillsOnGround : MonoBehaviour
{
    public delegate void groundSkillReset();

    public groundSkillReset skillReset;

    private CharacterController playerController;

    private void Start()
    {
        playerController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update ()
    {
        if (playerController.isGrounded)
        {
            if (skillReset != null)
                skillReset();
        }
	}
}