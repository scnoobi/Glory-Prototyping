﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AerialFire : MonoBehaviour
{
    private Transform cam;
    private ResetSkillsOnGround groundReset;

    private QuakePlayerMovement playerMovement;

    public float cooldown = 0.2f;
    private float timeTillNextSkill = 0;
    public float bulletSpeed = 5;
    public float bulletSpeedModifier = 2;
    public float weightModifier = 0.2f;

    public Transform instationPlace;

    public GameObject skill;

    #region Setup
    void Awake()
    {
        cam = GetComponent<InstantiateACamera>().GetCamera();
        groundReset = GetComponent<ResetSkillsOnGround>();
        playerMovement = GetComponent<QuakePlayerMovement>();
    }

    void OnEnable()
    {
        groundReset.skillReset += ResetOurAbility;
    }

    void OnDisable()
    {
        groundReset.skillReset -= ResetOurAbility;
    }

    private void ResetOurAbility()
    {
        timeTillNextSkill = 0;
    }
#endregion

    private void Update()
    {
        timeTillNextSkill -= Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (timeTillNextSkill <= 0)
            {
                timeTillNextSkill = cooldown;
                SkillUsage();
            }
        }
    }

    private void SkillUsage()
    {
        var clone = Instantiate(skill, cam.transform.position + cam.transform.forward * 0.7f,cam.transform.rotation);
        clone.GetComponent<Shot>().Fire(bulletSpeed,this);

        Fire(bulletSpeed);
    }

    private void Fire(float speed)
    {
        playerMovement.OutsideAcceleration(cam.transform.forward * -1, speed*bulletSpeedModifier, 14);
    }
}