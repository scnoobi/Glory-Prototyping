﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class QuickSort : MonoBehaviour {
    public static void Quicksort(List<Details> elements, int left, int right)
    {
        int i = left, j = right;
        var pivot = elements[(left + right) / 2].runningTime;

        while (i <= j)
        {
            while (elements[i].runningTime.CompareTo(pivot) < 0)
            {
                i++;
            }
            while (elements[j].runningTime.CompareTo(pivot) > 0)
            {
                j--;
            }

            if (i <= j)
            {
                // Swap
                var tmp = elements[i];
                elements[i] = elements[j];
                elements[j] = tmp;

                i++;
                j--;
            }
        }

        // Recursive calls
        if (left < j)
        {
            Quicksort(elements, left, j);
        }

        if (i < right)
        {
            Quicksort(elements, i, right);
        }
    }
}
