﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateACamera : MonoBehaviour
{
    public GameObject playerCamera;

    private Transform thePlayersCamera;

    private void Start()
    {
        if (thePlayersCamera == null)
            InstantiateCamera();
    }
    public Transform GetCamera()
    {
        if (thePlayersCamera == null)
        {
            InstantiateCamera();
        }
        return thePlayersCamera;
    }
    private void InstantiateCamera()
    {
        //Check if a camera already exists otherwise create a new one
        var tempCamera = GameObject.FindGameObjectWithTag("MainCamera");
        if (tempCamera == null)
        {
            thePlayersCamera = Instantiate(playerCamera).transform;
        }
        else
        {
            thePlayersCamera = tempCamera.transform;
        }   
    }
}
